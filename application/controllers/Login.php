<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        $this->load->model('ModelLogin', 'ModelLogin');
        // $this->msg = "Sorry, this site is Under Maintenance";

    }

    public function index()
    {
        // if ($this->session->userdata('id')) {
        //     redirect('dashboard');
        // }
        // $this->load->view('login/index');
        echo "hello wrljnbvef";
    }
    public function check_login()
    {
        $log = [];
        $data['username'] = htmlspecialchars($_POST['username']);
        $data['password'] = htmlspecialchars($_POST['password']);
        $user = $this->ModelLogin->islogin($data);

        if ($user) {

            $data = array(
                'id' => $user['id'],
                'username' => $user['username'],
                'level' => $user['level'],
                'status' => $user['status'],
                'id_universitas' => $user['id_universitas'],
            );

            $this->session->set_userdata($data);

            $log = [
            'status' => true,
            // local
            'url' => $_SERVER['HTTP_HOST'].'/smsbc_student/pages/',
            //  server
            // 'url' => 'http://150.242.111.235/smsbc_student/pages/',
            'session' => $data,
            ];

            //  OPEN :: LOG
            // $this->load->model('LogModel', 'lm');

            // $msgLog = "User : " . $user['username'] . " -> Login";
            // $this->lm->id_user = $user['id'];
            // $this->lm->inLogActivity($msgLog);
        } else {
            $log = [
                'status' => false,
                'msg' => 'Tidak dapat login, harap periksa kembali username dan password anda',
            ];
        }

        echo json_encode($log);
    }

    public function signOut()
    {
        //  OPEN :: LOG
        $this->load->model('LogModel', 'lm');

        $msgLog = "User : " . $this->session->userdata('username') . " -> Logout";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->session->sess_destroy();
        // $this->load->view('login/index');
        echo "
        <script>
        localStorage.clear();
        window.location.href = '" . site_url('/') . "';
        </script>
        ";
    }
}