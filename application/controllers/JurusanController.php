<?php

defined('BASEPATH') OR exit('No direct script
 access allowed');

class JurusanController extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
  }

  public function getJurusan($parameterJurusan=null)
  {
        // bisa pake  distinct(jurusan)  dan group by
    $bonitanew  = $this->load->database('bonitanew', TRUE);
    $idUniersitasLogin = $this->session->userdata('id_universitas');
    $query = $bonitanew->query("SELECT jurusan,university,fakultas FROM students_regist_new2  where university='$idUniersitasLogin' and fakultas='$parameterJurusan' group by jurusan");

    if ($query) {
      $result = array('success' => true, 'data' => $query->result());
    } else {
      $result = array('success' => false, 'msg' => 'Failed to fetch all data');
    }
    $result['debugq'] = $bonitanew->last_query();
    echo json_encode($result);
    

  }

    // public function getFakultas()
    // {
    //     $bonitanew  = $this->load->database('bonitanew', TRUE);
    //     $dataLogin = $this->session->userdata('id_universitas');
    //     $query = $bonitanew->query("SELECT students_faculty.idfaculty,students_faculty.name FROM students_regist_new2 INNER JOIN students_faculty ON students_regist_new2.fakultas = students_faculty.idfaculty where university='$dataLogin'  group by fakultas");


    //     if ($query) {
    //         $result = array('success' => true, 'data' => $query->result());
    //     } else {
    //         $result = array('success' => false, 'msg' => 'Failed to fetch all data ');
    //     }
    //     $result['debugq'] = $bonitanew->last_query();
    //     echo json_encode($result);

    // }

  public function getJurusanAll()
  {
    $bonitanew  = $this->load->database('bonitanew', TRUE);
    $query = $bonitanew->query("SELECT * FROM students_faculty")->result();
    echo json_encode($query);

  }


  public function getdataSessionUniversitas($username)
  {
    $query = $this->db->query("SELECT * FROM user WHERE username= '$username'")->row();
    echo json_encode($query);
  }

  public function getCurlFakulty()
  {
    $bonitanew  = $this->load->database('bonitanew', TRUE);
    $query = $bonitanew->query("SELECT * FROM students_faculty")->result();
    echo json_encode($query);
  }


  public function checkDataUserLogin()
  {

    $query = $this->db->query("SELECT * FROM user");
    if ($query) {
      $result = array('success' => true, 'data' => $query->result());
    } else {
      $result = array('success' => false, 'msg' => 'Failed to fetch all data user ');
    }
    $result['debugq'] = $this->db->last_query();
    echo json_encode($result);
  }


  public function getNreSpesifik()
  {
   $query = $this->db->query("SELECT * FROM `tbl_spesifik`");
   if ($query) {
    $result = array('success' => true, 'data' => $query->result());
  } else {
    $result = array('success' => false, 'msg' => 'Failed to fetch all data user ');
  }
  $result['debugq'] = $this->db->last_query();
  echo json_encode($result);

}



public function dataasal()
{
  $datalooping = ['1','2','3','4'];

  for ($i=0; $i < count($datalooping); $i++) { 
   $data =  $this->hh($datalooping[$i]); 
   echo $data ." <br>";
 }
}




function hh($data='')
{
         // persiapkan curl
            // $ch = curl_init(); 
            // switch ($data) {
            //     case '1':
            //         return "hello world 1";
            //         break;
            //     case '2':
            //         return "hello world 2";
            //         break;

            //     default:
            //         return "default case switch";
            //         break;
            // }
            // // set url 
            // curl_setopt($ch, CURLOPT_URL, "http://localhost/getBroadcastSms/index.php/JurusanController/getCurlFakulty");

            // // return the transfer as a string 
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

            // // $output contains the output string 
            // $output = curl_exec($ch); 

            // // tutup curl 
            // curl_close($ch);      

            // // menampilkan hasil curl
            // // echo $output;



            // foreach (json_decode($output,TRUE) as $row) {
            //      echo $row['idfaculty'];
            //      echo $row['name'];
            // }
        // echo $data;

  $this->load->library('curl');
  $result = $this->curl->simple_get('http://localhost/getBroadcastSms/index.php/JurusanController/getCurlFakulty');
        // echo $result;
        // count($result);

  foreach (json_decode($result,TRUE) as $row) {
            // echo $row['idfaculty'];
            // echo $row['name'];'
    switch ($data) {
      case $row['idfaculty']:
      return $row['name'];

      break;

      default:
      return "hello world default";
      break;
    }
  }
}




public function testing()
{

     // $link =  $_SERVER['HTTP_HOST'];
     // echo $link;
 echo $_SERVER['HTTP_HOST'].'/smsbc_student/pages/';
     // die("helo");
}


public function dataJurusanGrup()
{
 $bonitanew   = $this->load->database('bonitanew',TRUE);

 $query = $bonitanew->query("SELECT count(*) as total, jurusan,university,fakultas FROM students_regist_new2  where university='3' and fakultas in (15,17,6) group by jurusan");

 if ($query) {
  $result = array('success' => true, 'data' => $query->result());
} else {
  $result = array('success' => false, 'msg' => 'Failed to fetch all data user ');
}
$result['debugq'] = $bonitanew->last_query();
echo json_encode($result);

}


public function dataJurusandistinct()
{
 $bonitanew   = $this->load->database('bonitanew',TRUE);
 $iduniv =  $this->input->get('iduniv');
 $id = $this->input->get('id');

 $query = $bonitanew->query("SELECT 
  students_regist_new2.nre, students_regist_new2.university, students_regist_new2.fakultas,students_regist_new2.jurusan,tbl_jurusan.jurusan as jurusanjoin,tbl_jurusan.id as id_jurusan
  FROM
  students_regist_new2  
  left join 
  tbl_jurusan on tbl_jurusan.jurusan=students_regist_new2.jurusan
  where
  students_regist_new2.university='$iduniv' and students_regist_new2.fakultas in ($id)
  group by jurusan;");

 if ($query) {
  $result = array('success' => true, 'data' => $query->result());
} else {
  $result = array('success' => false, 'msg' => 'Failed to fetch all data user ');
}
$result['debugq'] = $bonitanew->last_query();
echo json_encode($result);

}



public function joinQuery()
{
  $bonitanew   = $this->load->database('bonitanew',TRUE);

  $query = $bonitanew->query("SELECT university,fakultas FROM students_regist_new2  where university='3' and fakultas in (15,17) group by jurusan");

  if ($query) {
    $result = array('success' => true, 'data' => $query->result());
  } else {
    $result = array('success' => false, 'msg' => 'Failed to fetch all data user ');
  }
  $result['debugq'] = $bonitanew->last_query();
  echo json_encode($result);

}

public function testingbos()
{

  $bonitanew   = $this->load->database('bonitanew',TRUE);
      // get data fakultas
      // $fakultasquery =  $bonitanew->query("SELECT idfaculty as id,name FROM students_faculty")->result();
      // echo json_encode($fakultasquery);
      // akhir get data fakultas

  $query =  $this->db->query("SELECT * FROM `tbl_smsbroadcast_student` where id= 108");
  if ($query) {
    $result = array('success' => true, 'data' => $query->row());
  } else {
    $result = array('success' => false, 'msg' => 'Failed to fetch all data user ');
  }
  $result['debugq'] = $this->db->last_query();
  echo json_encode($result);
}


public function getFakultas()
{


  $bonitanew  = $this->load->database('bonitanew', TRUE);
  $id = $this->input->get('dataId');

  if ($id == "NULL") {
        $query = $bonitanew->query("SELECT 
     students_faculty.idfaculty, students_faculty.name
     FROM
     students_regist_new2
     INNER JOIN
     students_faculty ON students_regist_new2.fakultas = students_faculty.idfaculty
     WHERE
     university = '3'
     GROUP BY fakultas");

   
   

   if ($query) {
    $result = array('success' => true, 'data' => $query->result());
  } else {
    $result = array('success' => false, 'msg' => 'Failed to fetch all data ');
  }
  $result['debugq'] = $bonitanew->last_query();
  echo json_encode($result);
  }else{
   $query = $bonitanew->query("SELECT 
     students_faculty.idfaculty, students_faculty.name
     FROM
     students_regist_new2
     INNER JOIN
     students_faculty ON students_regist_new2.fakultas = students_faculty.idfaculty
     WHERE
     university = '3'  AND students_faculty.idfaculty NOT IN ($id)
     GROUP BY fakultas");

   $datatampilkan = $bonitanew->query("SELECT idfaculty,name 
     FROM 
     students_faculty 
     where idfaculty IN($id)");


   if ($query) {
    $result = array('success' => true, 'data' => $query->result(),'datatampilkanchecked'=> $datatampilkan->result());
  } else {
    $result = array('success' => false, 'msg' => 'Failed to fetch all data ');
  }
  $result['debugq'] = $bonitanew->last_query();
  echo json_encode($result);
}


}


public function dataJurusandistinctedit()
{
 $bonitanew   = $this->load->database('bonitanew',TRUE);
 $iduniv =  $this->input->get('iduniv');
 $idfakultas = $this->input->get('idfakultas');
 $idedit = $this->input->get('idedit');
 $idjurusanedit = $this->input->get("idjurusanedit");
 if ($idjurusanedit == '') {
   $query = $bonitanew->query("SELECT 
    students_regist_new2.nre, students_regist_new2.university, students_regist_new2.fakultas,students_regist_new2.jurusan,tbl_jurusan.jurusan as jurusanjoin,tbl_jurusan.id as id_jurusan
    FROM
    students_regist_new2  
    left join 
    tbl_jurusan on tbl_jurusan.jurusan=students_regist_new2.jurusan
    where
    students_regist_new2.university='3' and students_regist_new2.fakultas IN  ($idfakultas)
    group by jurusan;");

   if ($query) {
    $result = array('success' => true, 'data' => $query->result());
  } else {
    $result = array('success' => false, 'msg' => 'Failed to fetch all data user ');
  }
  $result['debugq'] = $bonitanew->last_query();
  echo json_encode($result);
}else{
  $query = $bonitanew->query("SELECT 
    students_regist_new2.nre, students_regist_new2.university, students_regist_new2.fakultas,students_regist_new2.jurusan,tbl_jurusan.jurusan as jurusanjoin,tbl_jurusan.id as id_jurusan
    FROM
    students_regist_new2  
    left join 
    tbl_jurusan on tbl_jurusan.jurusan=students_regist_new2.jurusan
    where
    students_regist_new2.university='3' and students_regist_new2.fakultas IN  ($idfakultas) and tbl_jurusan.id NOT IN  ($idjurusanedit)
    group by jurusan;");

  $datatampilkan = $bonitanew->query("SELECT 
    students_regist_new2.nre, students_regist_new2.university, students_regist_new2.fakultas,students_regist_new2.jurusan,tbl_jurusan.jurusan as jurusanjoin,tbl_jurusan.id as id_jurusan
    FROM
    students_regist_new2  
    left join 
    tbl_jurusan on tbl_jurusan.jurusan=students_regist_new2.jurusan
    where
    students_regist_new2.university='3' and students_regist_new2.fakultas IN  ($idfakultas) and tbl_jurusan.id IN  ($idjurusanedit)
    group by jurusan;");

  if ($query) {
    $result = array('success' => true, 'data' => $query->result(),'datatampilkanchecked'=> $datatampilkan->result());
  } else {
    $result = array('success' => false, 'msg' => 'Failed to fetch all data user ');
  }

  $result['debugq'] = $bonitanew->last_query();
  echo json_encode($result);
}








}











}

/* End of file JurusanController.php */
