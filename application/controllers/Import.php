<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	private $filename = "kerjaan_baru";
    var $status = 0;

	public function index()
	{
		echo "helllo werod";
	}


    public function importBack() {

    $name = $this->input->post('namepesifik');	

    // Load plugin PHPExcel nya
    include APPPATH.'third_party/PHPExcel/PHPExcel.php';



    // Upload
    $this->filename = $this->upload();

    //insert ke table spesifik
    $namepesifik = $this->input->post('namepesifik');
    $description = $this->input->post('description');
    $created_by = $this->input->get('created_by');

    $tampungDataInsert = [
      'code_spesifik' => $namepesifik,
      'description'   => $description,
      'created_date'  => Date('Y-m-d H:i:s'),
      'created_by'    => $created_by,
    ];

    $this->db->insert('tbl_spesifik', $tampungDataInsert);

   

    $excelreader = new PHPExcel_Reader_Excel2007();
    $loadexcel = $excelreader->load('uploads/excel/'.$this->filename); // Load file yang telah diupload ke folder excel
    $getSheet = $loadexcel->getSheetNames();
    foreach($getSheet as $rows) {
        $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
        // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
        // var_dump($sheet); Buat sebuah variabel array untuk menampung array data yg
        // akan kita insert ke database
        $data = [];

        $numrow = 1;
        foreach($sheet as $row) {
            // Cek $numrow apakah lebih dari 1 Artinya karena baris pertama adalah nama-nama
            // kolom Jadi dilewat saja, tidak usah diimport
            if ($numrow > 1) {
                // Kita push (add) array data ke variabel data
                $obj = array(
                    // 'serialNumber' => $this->cekSn($row['A'], $row['B'])['sn'],
                    // Serial Number
                    'code_spesifik' => $this->input->post('namepesifik'),
                    'msisdn'        => $row['A'],
                    // 'refillId' => $rows,
                    // 'status' => 0,
                    // 'status_voucher' => $this->cekSn($row['A'],$row['B'])['status'],
                    // 'name' => $name,
                );
                array_push($data, $obj);
            }

            $numrow++; // Tambah 1 setiap kali looping
        }
        // var_dump($data); Panggil fungsi insert_multiple yg telah kita buat sebelumnya
        // di model
        try {
            $q = $this->db->insert_batch('tbl_msisdn_spesifik', $data);
            // $q = $this->dm->insert_multiple($data);
            if ($q) {
                $log = array(
                    'success' => true,
                    'title' => 'Success',
                    'msg' => 'Berhasil import'
                );
                // redirect("http://150.242.111.235/googleplay/");
            } else {
                $log = array(
                    'success' => false,
                    'title' => 'Failed',
                    'msg' => 'File Voucher already exists '
                );
                // redirect("http://150.242.111.235/googleplay?err=1");
            }
        } catch (Exception $th) {
            // echo $th->getMessage();
            $log = array(
                'success' => false,
                'msg' => 'Failed claused by '.$th->getMessage()
            );
            // redirect('http://localhost/googleplay?err=1');
        }
    }
    // Redirect ke halaman awal (ke controller siswa fungsi index)
    echo json_encode($log);
}


 public function cekSn($sn = "", $voucher = "")
    {
        $log =  [];
        if ($sn == "") {
            $sub =  $voucher[0].$voucher[1].$voucher[2].$voucher[3].$voucher[4].$voucher[5];
            $data = $sub;
            $log = [
                'sn' => $data,
                'status' => '0'
            ];
        } else {
            $data = $sn;
            $log = [
                'sn' => $data,
                'status' => '1'
            ];
        }

        return $log;
    }


    //upload excel
    public function upload()
    {
        $config['upload_path'] = "./uploads/excel";
        $config['allowed_types'] = 'xlsx|csv';
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload("file")) {
            $data = array('upload_data' => $this->upload->data());

            $image = $data['upload_data']['file_name'];
            return $image;
        }
    }

}

/* End of file Import.php */
/* Location: ./application/controllers/Import.php */